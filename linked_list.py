class Node:
    def __init__(self, data=None, next=None, prev=None):
        self.data = data
        self.next = next
        self.prev = prev

class DoublyLinkedList:
    def __init__(self):
        self.head = None

    def print_forward(self):
        if self.head is None:
            print("Linked list is empty")
            return

        iter = self.head
        list_str = ''
        while iter:
            list_str += str(iter.data) + ' --> '
            iter = iter.next
        print(list_str)

    def print_backward(self):
        if self.head is None:
            print("Linked list is empty")
            return

        last_node = self.get_last_node()
        iter = last_node
        list_str = ''
        while iter:
            list_str += iter.data + '-->'
            iter = iter.prev
        print("Link list in reverse: ", list_str)

    def get_last_node(self):
        iter = self.head
        while iter.next:
            iter = iter.next

        return iter

    def get_length(self):
        count = 0
        iter = self.head
        while iter:
            count += 1
            iter = iter.next

        return count

    def insert_at_begining(self, data):
        if self.head == None:
            node = Node(data, self.head, None)
            self.head = node
        else:
            node = Node(data, self.head, None)
            self.head.prev = node
            self.head = node

    def insert_at_end(self, data):
        if self.head is None:
            self.head = Node(data, None, None)
            return

        iter = self.head

        while iter.next:
            iter = iter.next

        iter.next = Node(data, None, iter)

    def insert_at(self, index, data):
        if index < 0 or index > self.get_length():
            raise Exception("Invalid Index")

        if index == 0:
            self.insert_at_begining(data)
            return

        count = 0
        iter = self.head
        while iter:
            if count == index - 1:
                node = Node(data, iter.next, iter)
                if node.next:
                    node.next.prev = node
                iter.next = node
                break

            iter = iter.next
            count += 1

    def remove_at(self, index):
        if index < 0 or index >= self.get_length():
            raise Exception("Invalid Index")

        if index == 0:
            self.head = self.head.next
            self.head.prev = None
            return

        count = 0
        iter = self.head
        while iter:
            if count == index:
                iter.prev.next = iter.next
                if iter.next:
                    iter.next.prev = iter.prev
                break

            iter = iter.next
            count += 1

    def insert_values(self, data_list):
        self.head = None
        for data in data_list:
            self.insert_at_end(data)


if __name__ == '__main__':
    print('empty')